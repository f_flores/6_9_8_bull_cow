//
// File name: bull_cow_ch6.cpp
// Description: Implements bulls and cows game. 
// Say the character sequence to be guessed is abcd and the user guesses wbxa; 
// the response should be “1 bull and 1 cow” because the user got one character ('b') right 
// and in the right position (a bull) and one letter (a) right but in the wrong position (a cow). 
// The guessing continues until the user gets four bulls, that is, has the four letter 
// match in the correct order.
//

#include "std_utils.h"
#include <limits.h>

const int Bull_number = 1234;
const int Size_Sequence = 4;
const int Max_Guess = 'z';
const int Min_Guess = 'a';
const int Set_Size = 26;
class OtherException{};

bool four_different(vector <char> &);
void populate_vector(string, vector <char> &);
void generate_chs(vector <char> &);
void init_vch(vector <char> &);
int get_bulls(vector<char> &, vector<char> &);
int get_cows(vector<char> &, vector<char> &);
void clear_input_routine(const string msg);

//
// clear std cin buffer
//
void clear_input_routine(const string error_msg)
{
  cout << error_msg << endl;
  cin.clear();
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

//
// clear vector of chars
//
void init_vch(vector <char> &v)
{
  for (auto c: v) c = 'a';
}


int main(){
  try{
    vector <char> bull_chs(Size_Sequence);
    vector <char> guess(Size_Sequence);
    string curr_guess = " ";
    bool found_4_bulls{ false };
    char play_again = 'y';
    int num_bulls {0}, num_cows {0};

    srand(time(NULL)); // initialize random seed

    do
    {
      generate_chs(bull_chs);
    }
    while (!four_different(bull_chs));
	   
    do
    {
      cout << "Guess a four lower-case character sequence (letters must be different): \n";
      if (!(cin >> curr_guess))
      {
        clear_input_routine("Only character sequence is allowed.");
        continue;
      }
      if (curr_guess.length() != Size_Sequence)
      {
	clear_input_routine("Sequence must be 4 characters long.");
	continue;
      }
      populate_vector(curr_guess, guess); // convert string to vector of characters
      if (four_different(guess))
      {
        num_bulls = get_bulls(bull_chs, guess);
	num_cows = get_cows(bull_chs, guess);
	if (num_bulls == Size_Sequence)
	{ // case of four bulls
	  cout << num_bulls << " bulls!!! \nCorrect Guess: " << curr_guess << endl;
	  found_4_bulls = true;
	  cout << "Play again? (y/n)" << endl;
	  if (!(cin >> play_again))
	  {
	    clear_input_routine("Must enter a valid character. Please try again.");
	    continue;
	  }
	  if (play_again == 'y')
	  {
	    do
            {
              generate_chs(bull_chs);
            }
            while (!four_different(bull_chs));
	  }
	}
	else
	  cout << num_bulls << " bull(s) and " << num_cows << " cow(s).\n";
      }
      else
      {
        cout << "\nFour characters must be different. Please enter a valid guess: ";
	found_4_bulls = false;
      }
    }
    while ((play_again == 'y') && cin.good());

    keep_window_open();
    return 0;
  }
  catch (runtime_error& e){
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    cerr << "Runtime error: " << e.what() << ".\n";
    keep_window_open();
    return 1;
  }
  catch (exception& e){
    cerr << "Exception Error: " << e.what() << ".\n";
    keep_window_open();
    return 2;
  }
  catch (OtherException){
    cerr << "Oops. Unknown Exception." << endl;
    keep_window_open();
    return 3;
  }
}

//
// Checks if series has Size_Sequence different chars, prerequisite for games bulls and cows
//  pre condition: x must be a Size_Sequence character sequence
bool four_different(vector <char> &v_ch){
  //  if (str.length() != Size_Sequence)
  //  error("Only strings of length four allowed.");
  // 
  bool different_chars = true;
  for (unsigned int i = 0; different_chars && (i < v_ch.size() - 1); ++i)
    for (unsigned int j = i + 1; different_chars && (j < v_ch.size()); ++j)
      if (v_ch[i] == v_ch[j]) different_chars = false;
  return different_chars;
}

//
// populate bull char vector, uses <vector> char(string begin, string end)
//  pre condition: vector must be of size Size_Bullnum (see consts defined)
void populate_vector(string s, vector <char> &vc)
{
  if (vc.size() != Size_Sequence)
    error("Out of range error. Vector of ints (populate_vector).");
  int tmp = 0;
  for (auto c : s) // convert string to vector of characters
  {
    vc[tmp] = c;
    tmp++;
  }
}

// 
// get number of bulls... match of digit and match of position
int get_bulls(vector<char> &bull, vector<char> &guess)
{
  int nbulls{ 0 };
  for (unsigned int i = 0; i < bull.size(); ++i)
    if (bull[i] == guess[i]) nbulls++;
  return nbulls;
}

//
// get number of cows
//  match of digit but in different position...
int get_cows(vector<char> &bull, vector<char> &guess){
  int ncows{ 0 };
  for (unsigned int i = 0; i < bull.size(); ++i)
    for (unsigned int j = 0; j < guess.size(); ++j)
      if ((i != j) && (bull[i] == guess[j])) ncows++;
  return ncows;
}

//
// generate random character sequence to be guessed
// each character must be different.
// pre condition: sequence cannot be longer than ten digits
void generate_chs(vector<char> &gen_ch){
  int tmp { 0 };
  if (gen_ch.size() > 10) error("Number must be less than or equal to 10 digits.");

  for (unsigned int i = 0; i < gen_ch.size(); ++i)
    gen_ch[i] = 'a' + (char)(rand() % Set_Size);
    
  //    cout << "random character sequence: ";
  //  for (auto c : gen_ch) cout << c << ' ';
  //  cout << endl;
}
//EOF bull_and_cow_ch6.cpp
