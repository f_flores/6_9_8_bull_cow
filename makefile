# Makefile
CXX = g++
CPPFLAGS = -std=c++14 -v -g -o
HEADERS = std_utils.h
SOURCES = bull_cow.cpp 

digs.o:
	${CXX} ${CPPFLAGS} bull_cow.o ${HEADERS} ${SOURCES}

clean :
	rm bull_cow.o
